const sqlite3 = require("sqlite3").verbose();
const path = require("path");

const dbPath = path.join(__dirname, "database.sqlite");
const db = new sqlite3.Database(dbPath, (err) => {
  if (err) {
    return console.error(err.message);
  }

  console.log("Connection to Database `database.sqlite` Sucessful!");
});

// const query = `CREATE TABLE IF NOT EXISTS Tasks (
//   Task_ID INTEGER PRIMARY KEY AUTOINCREMENT,
//   Title VARCHAR(256) NOT NULL,
//   Completed INTEGER DEFAULT 0
// );`;

// db.run(query, (err) => {
//   if (err) {
//     return console.error(err.message);
//   }

//   console.log("Query run successfully!");
// });

module.exports = db;
