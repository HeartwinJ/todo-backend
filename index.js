const express = require("express");
const app = express();
const cors = require("cors");

const PORT = process.env.PORT || 3000;

const db = require("./common/dbConn");

app.use(express.json());
app.use(cors());

const router = express.Router();

app.use("/api", router);

router.get("/", (req, res) => {
  console.log("GET /api/");
  res.json({
    success: true,
    msg: "Fetched Available APIs",
    apis: [
      "GET /api/tasks",
      "POST /api/tasks",
      "PUT /api/tasks/{postID}",
      "DELETE /api/tasks/{postID}",
    ],
  });
});

router.get("/tasks", (req, res) => {
  console.log("GET /api/tasks");
  const query = "SELECT * FROM Tasks ORDER BY Completed";
  db.all(query, (err, rows) => {
    if (err) {
      return res.status(400).json({ success: false, msg: err.message });
    }
    res.json({ success: true, tasks: rows });
  });
});

router.post("/tasks", (req, res) => {
  console.log("POST /api/tasks");

  if (!req.body.title) {
    return res
      .status(400)
      .json({ success: false, msg: "Title should be provided!" });
  }

  const query = "INSERT INTO Tasks (Title) VALUES (?);";
  db.run(query, req.body.title, (err) => {
    if (err) {
      return res.status(400).json({ success: false, msg: err.message });
    }
    res.json({ success: true, msg: "Task Added Successfully!" });
  });
});

router.put("/tasks/:task_id", (req, res) => {
  console.log(`PUT /api/tasks/${req.params.task_id}`);

  const query = "UPDATE Tasks SET Completed = 1 WHERE Task_ID = ?";
  db.run(query, req.params.task_id, (err) => {
    if (err) {
      return res.status(400).json({ success: false, msg: err.message });
    }
    res.json({ success: true, msg: "Task Updated Successfully!" });
  });
});

router.delete("/tasks/:task_id", (req, res) => {
  console.log(`DELETE /api/tasks/${req.params.task_id}`);

  const query = "DELETE FROM Tasks WHERE Task_ID = ?";
  db.run(query, req.params.task_id, (err) => {
    if (err) {
      return res.status(400).json({ success: false, msg: err.message });
    }
    res.json({ success: true, msg: "Task Deleted Successfully!" });
  });
});

app.listen(PORT, () => {
  console.log(`Listening on Port ${PORT}`);
});
